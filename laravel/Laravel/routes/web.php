<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@default');
Route::get('/register', 'AuthController@form');
Route::post('/send', 'AuthController@kirim');

Route::get('/table', function(){
	return view('page2.table');
});

Route::get('/data-tables', function(){
	return view('page2.datatables');
});

//CRUD data Cast

//Create data Cast


//Masuk Ke Form
Route::get('/cast/create', 'CastController@create');
//Kirim Data Ke Tabel Cast
Route::post('/cast', 'CastController@outputcreate');

//Read data Cast

//Tampil Semua Data Cast
Route::get('/cast', 'CastController@outputread');
//Tampil Data Cast Yang Dicek
Route::get('/cast/{id}', 'CastController@outputshow');

//Update data Cast
//Masuk Ke Form Update Berdasarkan ID
Route::get('/cast/{id}/edit', 'CastController@outputedit');
//Kirim Data Input Update Ke Tabel Cast  Berdasarkan ID
Route::put('/cast/{id}', 'CastController@outputupdate');

//delete data Cast


//Delete Data Input Update Ke Tabel Cast  Berdasarkan I
Route::delete('/cast/{id}', 'CastController@outputdel');