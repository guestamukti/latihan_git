@extends('layout.master')
@section('judul')
    Halaman Tambah Data Caster
@endsection

@section('content')

<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control" value="{{old('nama')}}">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control" value="{{old('umur')}}">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label>
    <textarea name="biodata" cols="30" rows="10" class="form-control">{{old('biodata')}}</textarea>
  </div>
  @error('biodata')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection