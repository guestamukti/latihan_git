@extends('layout.master')
@section('judul')
    Halaman Data Caster
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btm-sm mb-4">Tambah Data Caster</a>

<table class="table table-bordered">
  <thead>                  
    <tr>
      <th>No.</th>
      <th>Nama</th>
      <th>Detail</th>
      <!-- <th>Biodata</th> -->
    </tr>
  </thead>

  <tbody>
  	@forelse ($cast as $id => $item)
    <tr>
      <td>{{ $id + 1 }}</td>
      <td>{{ $item->nama }}</td>
      <td>
      	<form action="/cast/{{$item->id}}" method="post">
      	@csrf
      	<a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Cek</a>
      	<a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Ubah</a>
      	@method('delete')
      	 <input type="submit" value="delete" class="btn btn-danger btn-sm">
      	</form>
      </td>
     <!--  <td>{{ $item->umur }}</td> -->
     <!--  <td>{{ $item->bio }}</td> -->
    </tr>

    @empty
    <tr>
      <td>Tidak Ada Data Di Tabel Cast</td>
      </tr>
    @endforelse
  </tbody>
</table>

@endsection