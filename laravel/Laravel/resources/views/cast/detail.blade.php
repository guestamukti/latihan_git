@extends('layout.master')
@section('judul')
    Halaman Data Caster {{ $cast->nama}}
@endsection

@section('content')

<table class="table table-bordered">
  <tbody>                  
    <tr>
    	<td style="width: 10px"><p><b>Nama </b></p></td>
    	<td style="width: 10px"><p><b>:</b></p></td>
    	<td><p><b>{{$cast->nama}}</b></p></td>
    </tr>

    <tr>
    	<td style="width: 10px"><p><b>Umur </b></p></td>
    	<td style="width: 10px"><p><b>:</b></p></td>
    	<td><p><b>{{ $cast->umur}}</b></p></td>
    </tr>

    <tr>
    	<td style="width: 10px"><p><b>Biodata </b></p></td>
    	<td style="width: 10px"><p><b>:</b></p></td>
    	<td><p><b>{{ $cast->bio}}</b></p></td>
    </tr>
   
  </tbody>
</table>

<a href="/cast" class="btn btn-secondary btn-sm mt-4">Kembali</a>

@endsection