@extends('layout.master')
@section('judul')
    Halaman Daftar
@endsection

@section('content')
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>
		<form action="/send" method="post">
		@csrf
		<label>First Name :</label>
		<br><br>
		<input type="text" name="firstname">
		<br><br>
		<label>Last Name :</label>
		<br><br>
		<input type="text" name="lastname">
		<br><br>
		<label>Gender :</label>
		<br><br>
		<input type="radio">
		Male
		<br>
		<input type="radio">
		Female
		<br>
		<input type="radio">
		Other
		<br><br>
		<label>Nationality :</label>
		<select name="country">
			<option value="indonesian">Indonesian</option>
			<option value="indonesian">Malaysian</option>
			<option value="indonesian">Singaporean</option>
		</select>
		<br><br>
		<label>Language Spoken:</label>
		<br><br>
		<input type="checkbox" name="spoken">
		Bahasa Indonesia
		<br>
		<input type="checkbox" name="spoken">
		English
		<br>
		<input type="checkbox" name="spoken">
		Other
		<br><br>
		<label>Bio :</label>
		<br><br>

		<textarea name="message" rows="10" cols="30"></textarea>
		<br><br>
		<input type="submit" value="send">
		</form>
@endsection

