<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
    	return view('cast.create');
    }

    public function outputcreate(Request $request)
    {

    	$request->validate(
    	[
    	'nama' => 'required',
    	'umur' => 'required|integer',
    	'biodata' => 'required',
		],
		[
			'nama.required' => 'Nama Tidak Boleh Kosong !',
			'umur.required' => 'Umur Tidak Boleh Kosong !',
			'umur.integer' => 'Inputan Umur Harus Angka',
			'biodata.required' => 'Nama Tidak Boleh Kosong !'
		]
	);
    	
    	DB::table('cast')->insert(
    	[
    		'nama' => $request['nama'],
    		'umur' => $request['umur'],
    		'bio' => $request['biodata'],
    	]
		);

		return redirect('/cast');
        
    }

    public function outputread()
    {

    	$cast = DB::table('cast')->get();
		return view('cast.read', compact('cast'));
    }

    public function outputshow($id)
    {

    	$cast = DB::table('cast')->where('id', $id)->first();

		return view('cast.detail', compact('cast'));
    }

    public function outputedit($id)
    {

    	$cast = DB::table('cast')->where('id', $id)->first();

		return view('cast.update', compact('cast'));
    }

    public function outputupdate(Request $request, $id)
    {

    	$request->validate(
    	[
    	'nama' => 'required',
    	'umur' => 'required|integer',
    	'biodata' => 'required',
		],
		[
			'nama.required' => 'Nama Tidak Boleh Kosong !',
			'umur.required' => 'Umur Tidak Boleh Kosong !',
			'umur.integer' => 'Inputan Umur Harus Angka',
			'biodata.required' => 'Nama Tidak Boleh Kosong !',
		]
	);
    	
    	DB::table('cast')
    	->where('id', $id)
    	->update(
    	[
    		'nama' => $request['nama'],
    		'umur' => $request['umur'],
    		'bio' => $request['biodata'],
    	]
		);

		return redirect('/cast');
	}

	public function outputdel($id)
	{
		DB::table('cast')->where('id', $id)->delete();

		return redirect('/cast');
	}
}
